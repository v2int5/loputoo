package database;


import java.sql.*;
import java.util.ArrayList;


public class Database {

    public static void main(String[] args) {
        addUser("Test", "a", "b", "parool", "asd");

    }


    public static void addUser(String userName, String firstName, String lastName, String passwd, String email){

        try {
            String[] saltAndPass= database.PasswordHasher.securePassword(passwd);
            String password = saltAndPass[1];
            String salt = saltAndPass[0];


            Connection con = getConnection();
            PreparedStatement posted =
                    con.prepareStatement("INSERT INTO user (userName, firstName, lastName, passwd, passSalt, email) VALUES (?, ?, ?, ?, ?, ?)");
            posted.setString(1, userName);
            posted.setString(2, firstName);
            posted.setString(3, lastName);
            posted.setString(4, password);
            posted.setString(5, salt);
            posted.setString(6, email);
            posted.executeUpdate();

        }
        catch (Exception e){
            System.out.println(e);
        }
    }


    public static User logUserIn(String userName, String passwd) {
        try {
            String salt = getUserSalt(userName);
            if(salt != null){
                String hashedPassword = PasswordHasher.createPasswordWithSalt(passwd, salt);
                Connection con = getConnection();
                PreparedStatement statement =
                        con.prepareStatement("SELECT idUser, userName, firstName, lastName, email, lastLogin FROM user WHERE userName = '"+userName+"' AND passwd ='" + hashedPassword + "'" );
                ResultSet result = statement.executeQuery();
                result.next();
                return new User(result.getInt("idUser"), result.getString("userName"), result.getString("firstName"), result.getString("lastName"),
                         result.getString("email"), result.getString("lastLogin"));
            }



        }catch (Exception e){
            System.out.println(e);
        }
        return null;
    }

    public static String getUserSalt(String userName){
        try{
            Connection con = getConnection();
            PreparedStatement statement = con.prepareStatement("SELECT userName, passSalt FROM user WHERE userName = '"+userName+"'");
            ResultSet result = statement.executeQuery();
            if(result.next()){
                return result.getString("passSalt");
            }

        }catch (Exception e){
            System.out.println(e);
        }
        return null;
    }

    public static void changePassword(User user, String newPassword){
        try {
            String[] saltAndPass = PasswordHasher.securePassword(newPassword);
            Connection con = getConnection();
            PreparedStatement statement = con.prepareStatement("UPDATE user SET passwd = '"+ saltAndPass[1]+"', passSalt = '"+saltAndPass[0]+"' WHERE idUser ='"+user.getId()+"'");
            statement.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }




    public static Connection getConnection() throws Exception{
        try{
            String driver = "com.mysql.jdbc.Driver";
            String url = "jdbc:mysql://localhost:3306/demo";
            String username = "mihkel";
            String password = "PAROOL";
            Class.forName(driver);
            //System.out.println("Connected");

            return DriverManager.getConnection(url, username, password);

        } catch(Exception e){
            System.out.println(e);
        }

        return null;
    }

}
