package gui;

import database.Database;
import database.User;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;


public class HelloFX extends Application {

    //Programmi esimene aken, sisselogimine, võimalik registreerida
    //TODO: guest user, pole vaja sisse logida.
    //TODO: proovi tekitada võimalus keeli vahetada
    @Override
    public void start(Stage stage) {
        stage.setTitle("Programm");
        Label userName = new Label("Kasutajanimi: ");
        Label passwd = new Label("Parool: ");
        Label notification = new Label("");
        TextField userNameField = new TextField();
        PasswordField passwdField = new PasswordField();

        Button logInButton = new Button("Logi sisse");
        Button registerButton = new Button("Registreeri");
        logInButton.setOnAction(event -> {
            try {
                User user = Database.logUserIn(userNameField.getText(), passwdField.getText());
                if(user != null){
                    loggedInScene(stage, user);
                }else{
                    notification.setText("Vale kasutajanimi või parool");
                }


            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        registerButton.setOnAction(event -> {
            try {
                registerScene(stage);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        HBox userNameBox = new HBox();
        userNameBox.getChildren().addAll(userName, userNameField);
        userNameBox.setSpacing(10);
        HBox passwdBox = new HBox();
        passwdBox.getChildren().addAll(passwd, passwdField);
        passwdBox.setSpacing(10);
        HBox buttons = new HBox();
        buttons.getChildren().addAll(logInButton, registerButton);
        buttons.setSpacing(10);
        VBox vb = new VBox();
        vb.getChildren().addAll(userNameBox, passwdBox, buttons, notification);
        Scene scene = new Scene(vb, 640, 480);
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }

    //Scene, kus kasutaja saab end registreerida
    public void registerScene(Stage stage){
        Button goBackButton = new Button("Tagasi avalehele");
        Button registerButton = new Button("Registreeri");
        Label userName = new Label("Kasutajanimi:");
        Label firstName = new Label("Eesnimi: ");
        Label lastName = new Label("Perekonnanimi: ");
        Label password = new Label("Parool: ");
        Label repeatPassword = new Label("Korda parooli: ");
        Label email = new Label("e-mail: ");
        Label notification = new Label("");
        TextField userNameField = new TextField();
        TextField firstNameField = new TextField();
        TextField lastNameField = new TextField();
        PasswordField passwordField = new PasswordField();
        PasswordField repeatPasswordField = new PasswordField();
        TextField emailField = new TextField();
        HBox userNameBox = new HBox(userName, userNameField);
        HBox firstNameBox = new HBox(firstName, firstNameField);
        HBox lastNameBox = new HBox(lastName, lastNameField);
        HBox emailBox = new HBox(email, emailField);
        HBox passwordBox = new HBox(password, passwordField);
        HBox repeatPasswordBox = new HBox(repeatPassword, repeatPasswordField);
        VBox vb = new VBox();
        vb.getChildren().addAll(userNameBox, firstNameBox, lastNameBox, emailBox, passwordBox, repeatPasswordBox, goBackButton, registerButton, notification);

        goBackButton.setOnAction(event -> start(stage));


        //kontroll, kas sisestatud paroolid ühtivad ning seejärel saadetakse andmebaasi
        registerButton.setOnAction(event -> {
            try {
                if(userNameField.getText().equals("") || passwordField.getText().equals("") || emailField.getText().equals("")){
                    notification.setText("Kasutajanimi, parool ja e-mail on kohustuslik");
                }else if(!passwordField.getText().equals(repeatPasswordField.getText())){
                    notification.setText("Paroolid ei ühti");
                }else {

                    Database.addUser(userNameField.getText(), firstNameField.getText(), lastNameField.getText(),
                            passwordField.getText(), emailField.getText());
                    notification.setText("Kasutaja registreeritud");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        Scene registreeri = new Scene(vb,640, 480);
        stage.setScene(registreeri);
    }

    //aken, kus kasutaja saab enda andmeid muuta
    //TODO: lisa ka emaili muutmise võimalus
    public void userViewScene(Stage stage, User user){
        Label userName = new Label("Kasutajanimi: " + user.getUserName());
        Label name = new Label("Nimi: " + user.getFirstName() + " " + user.getLastName());
        Label passwd = new Label("Vaheta parooli: ");
        Label notification = new Label("");
        PasswordField changePasswdField = new PasswordField();
        PasswordField passwdAgainField = new PasswordField();
        Button confirmButton = new Button("Kinnita");
        confirmButton.setOnAction(event -> {
            if(changePasswdField.getText().equals(passwdAgainField.getText())) {

                try {
                    Database.changePassword(user, changePasswdField.getText());
                    notification.setText("Parool uuendatud");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }else{
                notification.setText("Paroolid ei ühti");
            }
        });
        Button logOutButton = new Button("tagasi");
        logOutButton.setOnAction(event -> {
            loggedInScene(stage, user);
        });
        VBox vb = new VBox();
        vb.getChildren().addAll(userName, name, passwd, changePasswdField, passwdAgainField, confirmButton, logOutButton, notification);

        Scene loggedIn = new Scene(vb, 640, 480);
        stage.setScene(loggedIn);
    }
    //TODO: leht, kus on võimalik valida, kas tahad minna ülesandeid lahendama, vaadata oma lahendatud ülesandeid, muuta oma kasutajaseadeid või logida välja.
    public void loggedInScene(Stage stage, User user){
        Label kasutajaLabel = new Label("Kasutaja: " + user.getUserName());
        Button newTaskButton = new Button("Uus ülesanne");
        Button completedTaskButton = new Button("Tehtud ülesanded");
        Button helpButton = new Button("Abi");
        Button settingsButton = new Button("Seaded");
        settingsButton.setOnAction(event -> {
            userViewScene(stage, user);
        });
        Button logOutButton = new Button("Logi välja");
        logOutButton.setOnAction(event -> {
            start(stage);
        });

        BorderPane borderPane = new BorderPane();

        //kasutajanimi + keeled
        HBox topHBox = new HBox();
        topHBox.getChildren().addAll(kasutajaLabel);

        //Uus ülesanne, tehtud ülesanded, abi

        HBox midHBox = new HBox();
        midHBox.getChildren().addAll(newTaskButton, completedTaskButton, helpButton);

        HBox botHBox = new HBox();
        botHBox.getChildren().addAll(logOutButton, settingsButton);

        borderPane.setTop(topHBox);
        borderPane.setCenter(midHBox);
        borderPane.setBottom(botHBox);
        Scene scene = new Scene(borderPane, 640, 480);
        stage.setScene(scene);

    }

}