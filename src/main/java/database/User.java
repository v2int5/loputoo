package database;

public class User {
    private int id;
    private String userName;
    private String firstName;
    private String lastName;
    private String email;
    private String lastLogIn;

    public User(int id, String userName, String firstName, String lastName,  String email, String lastLogIn) {
        this.id = id;
        this.userName = userName;
        this.firstName = firstName;
        this.lastName = lastName;

        this.email = email;
        this.lastLogIn = lastLogIn;
    }

    public String getUserName() {
        return userName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }



    public String getEmail() {
        return email;
    }

    public String getLastLogIn() {
        return lastLogIn;
    }



    public void setEmail(String email) {
        this.email = email;
    }

    public String toString(){
        return userName;
    }

    public int getId() {
        return id;
    }
}
